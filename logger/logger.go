package logger

type TestLog interface {
	CallLog(int)
}

// Logger, adds messages to msgs.
type get2chan struct {
	n    int
	msgs chan []string
}
type Logger struct {
	msgs    []string
	logchan chan string
	getchan chan *get2chan
}

func NewLogger() *Logger {
	l := new(Logger)
	l.logchan = make(chan string, 25)
	l.getchan = make(chan *get2chan)

	go l.doLog()
	return l
}

// Handle chans.
func (l *Logger) doLog() {
	for {
		select {
		// Get msg from logchan.
		case m := <-l.logchan:
			l.msgs = append(l.msgs, m)
		// Get N from getchan, put Last N to msgs
		case g := <-l.getchan:
			if g.n < len(l.msgs) {
				g.msgs <- l.msgs[len(l.msgs)-g.n:]
			} else {
				g.msgs <- l.msgs
			}
		}
	}
}

// Put msg to logchan.
func (l *Logger) Log(msg string) {
	l.logchan <- msg
}

// Get last N messages
func (l *Logger) GetLast(n int) []string {
	ret := make(chan []string)

	l.getchan <- &get2chan{n, ret}

	// Wait till doLog() handles getchan - return msgs.
	return <-ret
}
