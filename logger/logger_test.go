package logger

import (
	"testing"
	//	"time"
)

//type TestLog interface {
//	CallLog(int)
//}

func TestLogMethod(t *testing.T) {

	var msgs = []string{
		"Dante",
		"travels",
		"through",
		"the",
		"centre",
		"of",
		"the",
		"Earth",
		"in",
		"the",
		"Inferno",
	}

	l := NewLogger()

	for _, m := range msgs {
		l.Log(m)
	}

	// time.Sleep(time.Second)

	last := l.GetLast(3)
	for _, m := range last {
		t.Errorf(m)
	}
}
