;;; Directory Local Variables
;;; See Info node `(emacs) Directory Variables' for more information.

((go-mode . ((eval . (setq my-compile-command "gofmt -w * && go install ${PWD##$GOPATH/src/}"))
             (eval . (setq my-test-command "go test -v ${PWD##$GOPATH/src/}"))
              )))
