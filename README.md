# Demonstrations of some Golang ideas #


Try this

```
#!bash

go get bitbucket.org/egor7/play/<package>
go test bitbucket.org/egor7/play/<package>
```

## Packages

* logger - non trivial tests for go-routines

## Others

Also http://vk.com/go_lang is my Russian-language golang blog. You can find lots of examples there.